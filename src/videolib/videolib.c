/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 */
#include "videolib.h"


#include "fonts/msxfont8x8.c"
#include "fonts/vdfont6x8.c"


SDL_Window *sdl_win = NULL;
SDL_Renderer *sdl_rend = NULL;
SDL_Texture *sdl_scr = NULL; /* SDL_PIXELFORMAT_RGBA8888, 800x600 */
Uint32 *sdl_vscr = NULL; /* SDL_PIXELFORMAT_RGBA8888, 800x600 */


static void quit_cleanup (void) {
  if (sdl_vscr != NULL) free(sdl_vscr);
  if (sdl_scr != NULL) SDL_DestroyTexture(sdl_scr);
  if (sdl_rend != NULL) SDL_DestroyRenderer(sdl_rend);
  if (sdl_win != NULL) SDL_DestroyWindow(sdl_win);
  SDL_Quit();
}


int video_init (void) {
  static int atexit_set = 0;
  if (SDL_Init(SDL_INIT_VIDEO|SDL_INIT_EVENTS|SDL_INIT_TIMER) < 0) {
    fprintf(stderr, "FATAL: can't initialize SDL: %s\n", SDL_GetError());
    return -1;
  }
  if (!atexit_set) {
    atexit_set = 1;
    atexit(quit_cleanup);
  }
  //
  sdl_vscr = malloc(SCR_WIDTH*SCR_HEIGHT*sizeof(sdl_vscr[0]));
  if (sdl_vscr == NULL) {
    fprintf(stderr, "FATAL: can't create virtual screen buffer!\n");
    return -1;
  }
  //
  sdl_win = SDL_CreateWindow("StrokeTest", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCR_WIDTH, SCR_HEIGHT, 0/*SDL_WINDOW_OPENGL*/);
  if (sdl_win == NULL) {
    fprintf(stderr, "FATAL: can't create SDL window: %s\n", SDL_GetError());
    return -1;
  }
  //
  sdl_rend = SDL_CreateRenderer(sdl_win, -1, 0/*SDL_RENDERER_PRESENTVSYNC*/);
  if (sdl_rend == NULL) {
    fprintf(stderr, "FATAL: can't create SDL renderer: %s\n", SDL_GetError());
    return -1;
  }
  //
  SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "nearest"); // "nearest" or "linear"
  SDL_RenderSetLogicalSize(sdl_rend, SCR_WIDTH, SCR_HEIGHT);
  //
  sdl_scr = SDL_CreateTexture(sdl_rend, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_STREAMING, SCR_WIDTH, SCR_HEIGHT);
  if (sdl_scr == NULL) {
    fprintf(stderr, "FATAL: can't create SDL texture: %s\n", SDL_GetError());
    return -1;
  }
  /*
  SDL_EnableKeyRepeat(200, 25);
  SDL_EnableUNICODE(1);
  */
  return 0;
}


void paint_screen (void) {
  /*
  SDL_SetRenderDrawColor(sdl_rend, 0, 0, 0, 255);
  SDL_RenderClear(sdl_rend);
  */
  SDL_UpdateTexture(sdl_scr, NULL, sdl_vscr, SCR_WIDTH*sizeof(sdl_vscr[0]));
  SDL_RenderCopy(sdl_rend, sdl_scr, NULL, NULL);
  SDL_RenderPresent(sdl_rend);
}


void draw_line (int x0, int y0, int x1, int y1, Uint32 col) {
  int dx =  abs(x1-x0), sx = (x0 < x1 ? 1 : -1);
  int dy = -abs(y1-y0), sy = (y0 < y1 ? 1 : -1);
  int err = dx+dy, e2; /* error value e_xy */
  for (;;) {
    put_pixel(x0, y0, col);
    if (x0 == x1 && y0 == y1) break;
    e2 = 2*err;
    if (e2 >= dy) { err += dy; x0 += sx; } /* e_xy+e_x > 0 */
    if (e2 <= dx) { err += dx; y0 += sy; } /* e_xy+e_y < 0 */
  }
}


void fill_rect (int x, int y, int w, int h, Uint32 col) {
  if (w > 0 && h > 0 && x < SCR_WIDTH && y < SCR_HEIGHT && x+w >= 0 && y+h >= 0) {
    SDL_Rect r, sr, dr;
    sr.x = 0; sr.y = 0; sr.w = SCR_WIDTH; sr.h = SCR_HEIGHT;
    r.x = x; r.y = y; r.w = w; r.h = h;
    if (SDL_IntersectRect(&sr, &r, &dr)) {
      Uint32 a = dr.y*SCR_WIDTH+dr.x;
      while (dr.h-- > 0) {
        Uint32 da = a;
        for (int f = dr.w; f > 0; --f) sdl_vscr[da++] = col;
        a += SCR_WIDTH;
      }
    }
  }
}


void draw_rect (int x, int y, int w, int h, Uint32 col) {
  if (w > 0 && h > 0) {
    draw_line(x, y, x+w-1, y, col);
    draw_line(x, y+h-1, x+w-1, y+h-1, col);
    draw_line(x, y, x, y+h-1, col);
    draw_line(x+w-1, y, x+w-1, y+h-1, col);
  }
}


void draw_char6 (int x, int y, char ch, Uint32 col, Uint32 bkcol) {
  int pos = (ch&0xff)*8;
  for (int dy = 0; dy < 8; ++dy) {
    unsigned char b = font6x8[pos++];
    for (int dx = 0; dx < 6; ++dx) {
      if (b&0x80) {
        if (col != TRANSPARENT_COLOR) {
          put_pixel(x+dx*2+0, y+dy*2+0, col);
          put_pixel(x+dx*2+1, y+dy*2+0, col);
          put_pixel(x+dx*2+0, y+dy*2+1, col);
          put_pixel(x+dx*2+1, y+dy*2+1, col);
        }
      } else if (bkcol != TRANSPARENT_COLOR) {
        put_pixel(x+dx*2+0, y+dy*2+0, bkcol);
        put_pixel(x+dx*2+1, y+dy*2+0, bkcol);
        put_pixel(x+dx*2+0, y+dy*2+1, bkcol);
        put_pixel(x+dx*2+1, y+dy*2+1, bkcol);
      }
      b = (b&0x7f)<<1;
    }
  }
}


void draw_str6 (int x, int y, const char *str, Uint32 col, Uint32 bkcol) {
  if (!str) return;
  while (*str) {
    draw_char6(x, y, *str++, col, bkcol);
    x += 12;
  }
}


void draw_char8 (int x, int y, char ch, Uint32 col, Uint32 bkcol) {
  int pos = (ch&0xff)*8;
  for (int dy = 0; dy < 8; ++dy) {
    unsigned char b = msxfont8x8[pos++];
    for (int dx = 0; dx < 8; ++dx) {
      if (b&0x80) {
        if (col != TRANSPARENT_COLOR) {
          put_pixel(x+dx*2+0, y+dy*2+0, col);
          put_pixel(x+dx*2+1, y+dy*2+0, col);
          put_pixel(x+dx*2+0, y+dy*2+1, col);
          put_pixel(x+dx*2+1, y+dy*2+1, col);
        }
      } else if (bkcol != TRANSPARENT_COLOR) {
        put_pixel(x+dx*2+0, y+dy*2+0, bkcol);
        put_pixel(x+dx*2+1, y+dy*2+0, bkcol);
        put_pixel(x+dx*2+0, y+dy*2+1, bkcol);
        put_pixel(x+dx*2+1, y+dy*2+1, bkcol);
      }
      b = (b&0x7f)<<1;
    }
  }
}


void draw_str8 (int x, int y, const char *str, Uint32 col, Uint32 bkcol) {
  if (!str) return;
  while (*str) {
    draw_char8(x, y, *str++, col, bkcol);
    x += 16;
  }
}


const char *read_str (void) {
  static char buf[128];
  memset(buf, 0, sizeof(buf));
  SDL_StartTextInput();
  for (;;) {
    SDL_Event event;
    fill_rect(0, 0, SCR_WIDTH, 20, rgb2col(0, 0, 255));
    draw_str6(2, 2, buf, rgb2col(255, 255, 0), TRANSPARENT_COLOR);
    fill_rect(2+12*strlen(buf), 0, 12, 20, rgb2col(255, 255, 255));
    paint_screen();
    if (SDL_WaitEvent(&event)) {
      switch (event.type) {
        case SDL_QUIT:
          SDL_StopTextInput();
          return READSTR_QUIT;
        case SDL_KEYDOWN:
          //ctrlDown = event.key.keysym.mod&KMOD_CTRL;
          switch (event.key.keysym.sym) {
            case SDLK_ESCAPE:
              SDL_StopTextInput();
              return NULL;
            case SDLK_BACKSPACE:
              if (strlen(buf) > 0) buf[strlen(buf)-1] = '\0';
              break;
            case SDLK_RETURN:
            case SDLK_KP_ENTER:
              SDL_StopTextInput();
              return buf;
          }
          break;
        case SDL_TEXTINPUT:
          if (!event.text.text[1] && (unsigned char)(event.text.text[0]) >= 32 && (unsigned char)(event.text.text[0]) < 127) {
            if (strlen(buf) < sizeof(buf)+1) {
              buf[strlen(buf)+1] = '\0';
              buf[strlen(buf)] = event.text.text[0];
            }
          }
          break;
      }
    }
  }
}
