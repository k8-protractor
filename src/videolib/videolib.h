/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 */
#ifndef __VIDEOLIB_H__
#define __VIDEOLIB_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <SDL.h>


/******************************************************************************/
#define TRANSPARENT_COLOR  (0x010203U)

#define SCR_WIDTH   (800)
#define SCR_HEIGHT  (600)


extern SDL_Window *sdl_win;
extern SDL_Renderer *sdl_rend;
extern Uint32 *sdl_vscr; /* SDL_PIXELFORMAT_RGBA8888, 800x600 */


/******************************************************************************/
extern int video_init (void);

extern void paint_screen (void);

extern void draw_line (int x0, int y0, int x1, int y1, Uint32 col);
extern void draw_rect (int x, int y, int w, int h, Uint32 col);
extern void fill_rect (int x, int y, int w, int h, Uint32 col);

extern void draw_char6 (int x, int y, char ch, Uint32 col, Uint32 bkcol);
extern void draw_str6 (int x, int y, const char *str, Uint32 col, Uint32 bkcol);

extern void draw_char8 (int x, int y, char ch, Uint32 col, Uint32 bkcol);
extern void draw_str8 (int x, int y, const char *str, Uint32 col, Uint32 bkcol);

#define READSTR_QUIT  ((const char *)1)
extern const char *read_str (void);


/******************************************************************************/
static inline Uint32 rgb2col (Uint8 r, Uint8 g, Uint8 b) { return (r<<24)|(g<<16)|(b<<8); }
static inline Uint32 rgba2col (Uint8 r, Uint8 g, Uint8 b, Uint8 a) { return (r<<24)|(g<<16)|(b<<8)|(a); }


/******************************************************************************/
static inline void put_pixel (int x, int y, Uint32 col) { if (x >= 0 && y >= 0 && x < SCR_WIDTH && y < SCR_HEIGHT) sdl_vscr[y*SCR_WIDTH+x] = col; }


#ifdef __cplusplus
}
#endif
#endif
