/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 *
 * Protractor recognizer
 */
#ifndef __GESTURENG_H__
#define __GESTURENG_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdlib.h>
#include <string.h>


/******************************************************************************/
/* DO NOT CHANGE! */
#define GENG_PTS_PER_STK  (16)  /* paper says that this is enough for protractor to work ok */


/******************************************************************************/
typedef struct {
  int *points; /* [0]:x, [1]:y, [2]:x, [3]:y, etc... */
  /* actually, coordinate pairs */
  int count;
  int points_alloted; /* ==GENG_PTS_PER_STK: points points to ptnorm */
} gengesture_t;


/* for protractor */
typedef float gengtemplate_t[GENG_PTS_PER_STK*2];

static inline size_t geng_tplsize (void) { return sizeof(gengtemplate_t[0])*GENG_PTS_PER_STK*2; }
static inline void geng_copytpl (gengtemplate_t dest, const gengtemplate_t src) { memmove(dest, src, sizeof(dest[0])*GENG_PTS_PER_STK*2); }


/******************************************************************************/
/* for all: <0: error; 0: ok */
extern int geng_init (gengesture_t *gg);
extern void geng_done (gengesture_t *gg);
extern void geng_clear (gengesture_t *gg);
/* ggdest must be initialized and must not be equal to ggsrc */
extern int geng_clone (gengesture_t *ggdest, const gengesture_t *ggsrc);

extern int geng_add_point (gengesture_t *gg, int x, int y);

extern int geng_normalize (gengtemplate_t tpldest, const gengesture_t *gg);

/* returns protractor matching value; -1: error */
extern double geng_match (const gengtemplate_t tpl, const gengtemplate_t sample);


static inline int geng_ptx (const gengesture_t *gg, int idx) { return gg->points[idx*2+0]; }
static inline int geng_pty (const gengesture_t *gg, int idx) { return gg->points[idx*2+1]; }


/******************************************************************************/
/* WARNING: we can have more than one template for the given name! */
/* WARNING: this structure will probably change in the future! */
typedef struct {
  int count;
  int capacity;
  gengtemplate_t *tpls;
  char **names;
} genglib_t;


/******************************************************************************/
extern int genglib_init (genglib_t *lib);
extern void genglib_done (genglib_t *lib);
extern void genglib_clear (genglib_t *lib);

/* add new template with the given name, NOT REPLACE */
extern int genglib_add_stk (genglib_t *lib, const char *name, const gengesture_t *gg);
/* add new template with the given name, NOT REPLACE */
extern int genglib_add_tpl (genglib_t *lib, const char *name, const gengtemplate_t tpl);
/* remove ALL templates for the given name; name is case-sensitive */
extern void genglib_remove (genglib_t *lib, const char *name);
/* return either name or NULL; if 'score' is not NULL, store result of geng_match() in it */
extern const char *genglib_find_match (const genglib_t *lib, const gengesture_t *gg, double *score);

extern int genglib_load (genglib_t *lib, FILE *fl);
extern int genglib_save (const genglib_t *lib, FILE *fl);
extern int genglib_load_file (genglib_t *lib, const char *name);
extern int genglib_save_file (const genglib_t *lib, const char *name);


#ifdef __cplusplus
}
#endif
#endif
