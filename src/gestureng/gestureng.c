/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 *
 * Protractor recognizer
 */
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "gestureng.h"


////////////////////////////////////////////////////////////////////////////////
#define MIN_DISTANCE  (4)  /* squared */


////////////////////////////////////////////////////////////////////////////////
/* ignore possible overflows here */
static inline double distance (const double x0, const double y0, const double x1, const double y1) {
  const double dx = x1-x0;
  const double dy = y1-y0;
  return sqrt(dx*dx+dy*dy);
}


////////////////////////////////////////////////////////////////////////////////
int geng_init (gengesture_t *gg) {
  if (gg != NULL) {
    memset(gg, 0, sizeof(*gg));
    return 0;
  }
  return -1;
}


void geng_done (gengesture_t *gg) {
  if (gg != NULL) {
    if (gg->points != NULL) free(gg->points);
    memset(gg, 0, sizeof(*gg));
  }
}


void geng_clear (gengesture_t *gg) {
  geng_done(gg);
}


int geng_clone (gengesture_t *ggdest, const gengesture_t *ggsrc) {
  if (ggdest != NULL && ggsrc != NULL) {
    geng_clear(ggdest);
    if ((ggdest->count = ggdest->points_alloted = ggsrc->count) > 0) {
      ggdest->points = malloc(sizeof(ggdest->points[0])*2*ggdest->count);
      if (ggdest->points == NULL) return -1;
      memcpy(ggdest->points, ggsrc->points, sizeof(ggdest->points[0])*2*ggdest->count);
    }
    return 0;
  }
  return -1;
}


int geng_add_point (gengesture_t *gg, int x, int y) {
  if (gg != NULL) {
    if (gg->count > 0) {
      /* check distance and don't add points that are too close to each other */
      int lx = geng_ptx(gg, gg->count-1)-x, ly = geng_pty(gg, gg->count-1)-y;
      if (lx*lx+ly*ly < MIN_DISTANCE*MIN_DISTANCE) return 0;
    }
    if (gg->count+1 > gg->points_alloted) {
      int nsz = ((gg->count+1)|0x3ff)+1;
      int *np = realloc(gg->points, sizeof(gg->points[0])*2*nsz);
      if (np == NULL) return -1;
      gg->points_alloted = nsz;
      gg->points = np;
    }
    gg->points[gg->count*2+0] = x;
    gg->points[gg->count*2+1] = y;
    ++gg->count;
    return 0;
  }
  return -1;
}


////////////////////////////////////////////////////////////////////////////////
static double geng_length (const gengesture_t *gg) {
  double len = 0.0;
  if (gg != NULL && gg->count >= 2) {
    double px = geng_ptx(gg, 0), py = geng_pty(gg, 0);
    for (int f = 1; f < gg->count; ++f) {
      const double cx = geng_ptx(gg, f), cy = geng_pty(gg, f);
      len += distance(px, py, cx, cy);
      px = cx;
      py = cy;
    }
  }
  return len;
}


/* ggdest should be initialized */
static int geng_resample (gengtemplate_t ptres, const gengesture_t *ggsrc) {
  double I = geng_length(ggsrc)/(GENG_PTS_PER_STK-1); // interval length
  double D = 0.0, prx, pry;
  int ptidx = 2;
  prx = geng_ptx(ggsrc, 0);
  pry = geng_pty(ggsrc, 0);
  /* add first point as-is */
  ptres[0] = prx;
  ptres[1] = pry;
  for (int f = 1; f < ggsrc->count; ) {
    double cx = geng_ptx(ggsrc, f), cy = geng_pty(ggsrc, f);
    double d = distance(prx, pry, cx, cy);
    if (D+d >= I) {
      const double dd = (I-D)/d;
      const double qx = prx+dd*(cx-prx);
      const double qy = pry+dd*(cy-pry);
      if (ptidx >= GENG_PTS_PER_STK*2) { fprintf(stderr, "ERR0!\n"); abort(); } /* the thing that should not be */
      ptres[ptidx++] = qx;
      ptres[ptidx++] = qy;
      /* use 'q' as previous point */
      prx = qx;
      pry = qy;
      D = 0.0;
    } else {
      D += d;
      prx = cx;
      pry = cy;
      ++f;
    }
  }
  /*fprintf(stderr, "NORM: orig_count=%d; count=%d\n", ggsrc->count, ptidx/2);*/
  /* somtimes we fall a rounding-error short of adding the last point, so add it if so */
  if (ptidx/2 == GENG_PTS_PER_STK-1) {
    ptres[ptidx++] = geng_ptx(ggsrc, ggsrc->count-1);
    ptres[ptidx++] = geng_pty(ggsrc, ggsrc->count-1);
  }
  if (ptidx != GENG_PTS_PER_STK*2) { fprintf(stderr, "ERR1!\n"); abort(); } /* the thing that should not be */
  return 0;
}


////////////////////////////////////////////////////////////////////////////////
/* stroke can be not centered, but must be resampled */
static int geng_vectorize (gengtemplate_t vres, const gengtemplate_t ptx, int orientation_sensitive)  {
  gengtemplate_t pts;
  double ind_angle, delta, sum, cosd, sind, magnitude;
  double cx = 0.0, cy = 0.0;
  /* center it */
  for (int f = 0; f < GENG_PTS_PER_STK; ++f) {
    cx += ptx[f*2+0];
    cy += ptx[f*2+1];
  }
  cx /= GENG_PTS_PER_STK;
  cy /= GENG_PTS_PER_STK;
  for (int f = 0; f < GENG_PTS_PER_STK; ++f) {
    pts[f*2+0] = ptx[f*2+0]-cx;
    pts[f*2+1] = ptx[f*2+1]-cy;
  }
  ind_angle = atan2(pts[1], pts[0]); /* always must be done for centered stroke */
  if (orientation_sensitive) {
    const double base_orientation = (M_PI/4)*floor((ind_angle+M_PI/8)/(M_PI/4));
    delta = base_orientation-ind_angle;
  } else {
    delta = ind_angle;
  }
  cosd = cos(delta);
  sind = sin(delta);
  sum = 0.0;
  for (int f = 0; f < GENG_PTS_PER_STK; ++f) {
    const double nx = pts[f*2+0]*cosd-pts[f*2+1]*sind;
    const double ny = pts[f*2+1]*cosd+pts[f*2+0]*sind;
    vres[f*2+0] = nx;
    vres[f*2+1] = ny;
    sum += nx*nx+ny*ny;
  }
  magnitude = sqrt(sum);
  for (int f = 0; f < GENG_PTS_PER_STK*2; ++f) vres[f] /= magnitude;
  return 0;
}


static double geng_optimal_cosine_distance (const gengtemplate_t v0, const gengtemplate_t v1) {
  double a = 0.0, b = 0.0, angle;
  for (int f = 0; f < GENG_PTS_PER_STK*2; f += 2) {
    a += v0[f]*v1[f+0]+v0[f+1]*v1[f+1];
    b += v0[f]*v1[f+1]-v0[f+1]*v1[f];
  }
  angle = atan(b/a);
  return acos(a*cos(angle)+b*sin(angle));
}


int geng_normalize (gengtemplate_t tpldest, const gengesture_t *gg) {
  if (gg != NULL && gg->count > 1) {
    gengtemplate_t pts;
    geng_resample(pts, gg);
    geng_vectorize(tpldest, pts, 1);
    return 0;
  }
  return -1;
}


double geng_match (const gengtemplate_t tpl, const gengtemplate_t sample) {
  return 1.0/geng_optimal_cosine_distance(tpl, sample);
}


////////////////////////////////////////////////////////////////////////////////
int genglib_init (genglib_t *lib) {
  if (lib != NULL) {
    memset(lib, 0, sizeof(*lib));
    return 0;
  }
  return -1;
}


void genglib_done (genglib_t *lib) {
  genglib_clear(lib);
}


void genglib_clear (genglib_t *lib) {
  if (lib != NULL) {
    for (int f = lib->count-1; f >= 0; --f) if (lib->names[f] != NULL) free(lib->names[f]);
    if (lib->names != NULL) free(lib->names);
    if (lib->tpls != NULL) free(lib->tpls);
    memset(lib, 0, sizeof(*lib));
  }
}


int genglib_add_stk (genglib_t *lib, const char *name, const gengesture_t *gg) {
  if (lib != NULL && name != NULL && gg != NULL && gg->count > 1) {
    gengtemplate_t t;
    geng_normalize(t, gg);
    return genglib_add_tpl(lib, name, t);
  }
  return -1;
}


int genglib_add_tpl (genglib_t *lib, const char *name, const gengtemplate_t tpl) {
  if (lib != NULL && name != NULL) {
    // append
    if (lib->count+1 > lib->capacity) {
      int nsz = ((lib->count+1)|0x3f)+1;
      gengtemplate_t *nt = realloc(lib->tpls, sizeof(lib->tpls[0])*nsz);
      char **nn = realloc(lib->names, sizeof(lib->names[0])*nsz);
      if (nt == NULL || nn == NULL) {
        if (nt != NULL) free(nt);
        return -1;
      }
      lib->capacity = nsz;
      lib->tpls = nt;
      lib->names = nn;
    }
    if ((lib->names[lib->count] = strdup(name)) == NULL) return -1;
    memmove(lib->tpls[lib->count], tpl, sizeof(tpl[0])*GENG_PTS_PER_STK*2);
    ++lib->count;
    return 0;
  }
  return -1;
}


void genglib_remove (genglib_t *lib, const char *name) {
  if (lib != NULL && name != NULL) {
    int f = 0;
    while (f < lib->count) {
      if (strcmp(lib->names[f], name) == 0) {
        int left = lib->count-f-1;
        free(lib->names[f]);
        memmove(lib->names+f, lib->names+f+1, sizeof(lib->names[0])*left);
        memmove(lib->tpls+f, lib->tpls+f+1, sizeof(lib->tpls[0])*left);
        --lib->count;
      } else {
        ++f;
      }
    }
  }
}


/* return either name or NULL */
const char *genglib_find_match (const genglib_t *lib, const gengesture_t *gg, double *score) {
  if (score != NULL) *score = NAN;
  if (lib != NULL && gg != NULL && gg->count > 1) {
    const char *name = NULL;
    double best_score = -INFINITY;
    gengtemplate_t smp;
    geng_normalize(smp, gg);
    for (int f = 0; f < lib->count; ++f) {
      double mt = geng_match(lib->tpls[f], smp);
      if (isnormal(mt) && mt > best_score) {
        best_score = mt;
        name = lib->names[f];
      }
    }
    if (name != NULL && best_score < 1.5) name = NULL;
    if (score != NULL && name != NULL) *score = best_score;
    return name;
  }
  return NULL;
}


////////////////////////////////////////////////////////////////////////////////
static int write_int (FILE *fl, int v) {
  unsigned int vv = (unsigned int)v;
  unsigned char buf[4];
  if (v < 0) return -1;
  for (int f = 0; f < 4; ++f, vv >>= 8) buf[f] = vv&0xff;
  return (fwrite(buf, 4, 1, fl) != 1 ? -1 : 0);
}


/* we have no negative numbers, so -1 is error */
static int read_int (FILE *fl) {
  unsigned int vv = 0;
  unsigned char buf[4];
  if (fread(buf, 4, 1, fl) != 1) return -1;
  for (int f = 0; f < 4; ++f) vv |= ((unsigned int)buf[f])<<(f*8);
  if (vv >= 0x7fffffffU) return -1;
  return (int)vv;
}


/* return NAN on error */
static float read_float (FILE *fl) {
  unsigned char buf[sizeof(float)];
  float num;
#if __FLOAT_WORD_ORDER__ == __ORDER_LITTLE_ENDIAN__
  if (fread(buf, sizeof(float), 1, fl) != 1) return NAN;
#else
  for (size_t f = sizeof(float); f > 0; --f) if (fread(&buf[f-1], 1, 1, fl) != 1) return NAN;
#endif
  memcpy(&num, buf, sizeof(float));
  return num;
}


static int write_float (FILE *fl, const float num) {
  unsigned char buf[sizeof(float)];
  memcpy(buf, &num, sizeof(float));
#if __FLOAT_WORD_ORDER__ == __ORDER_LITTLE_ENDIAN__
  if (fwrite(buf, sizeof(float), 1, fl) != 1) return -1;
#else
  for (size_t f = sizeof(float); f > 0; --f) if (fwrite(&buf[f-1], 1, 1, fl) != 1) return -1;
#endif
  return 0;
}


////////////////////////////////////////////////////////////////////////////////
#define SIGN  "K8PTRDB0"


int genglib_load (genglib_t *lib, FILE *fl) {
  int cnt, ssz = 0;
  char sign[8], *sbuf = NULL;
  if (fl == NULL || lib == NULL) goto error;
  if (fread(sign, 8, 1, fl) != 1) goto error;
  if (memcmp(sign, SIGN, 8) != 0) goto error;
  if ((cnt = read_int(fl)) < 0) goto error;
  genglib_clear(lib);
  while (cnt-- > 0) {
    int len;
    gengtemplate_t t;
    /* name */
    if ((len = read_int(fl)) < 0) goto error;
    if (ssz < len+1) {
      char *n = realloc(sbuf, len+1);
      if (n == NULL) goto error;
      sbuf = n;
    }
    if (len > 0 && fread(sbuf, len, 1, fl) != 1) goto error;
    sbuf[len] = 0;
    /* template */
    for (int c = 0; c < GENG_PTS_PER_STK*2; ++c) if (isnan((t[c] = read_float(fl)))) goto error;
    /* add it */
    if (genglib_add_tpl(lib, sbuf, t) < 0) goto error;
  }
  return 0;
error:
  if (sbuf != NULL) free(sbuf);
  genglib_clear(lib);
  return -1;
}


int genglib_save (const genglib_t *lib, FILE *fl) {
  if (fl != NULL && lib != NULL) {
    if (fwrite(SIGN, 8, 1, fl) != 1) return -1;
    if (write_int(fl, lib->count) < 0) return -1;
    for (int f = 0; f < lib->count; ++f) {
      /* name */
      int len = strlen(lib->names[f]);
      if (write_int(fl, len) < 0) return -1;
      if (len > 0 && fwrite(lib->names[f], len, 1, fl) != 1) return -1;
      /* template */
      for (int c = 0; c < GENG_PTS_PER_STK*2; ++c) if (write_float(fl, lib->tpls[f][c]) < 0) return -1;
    }
    /* done */
    return 0;
  }
  return -1;
}


int genglib_load_file (genglib_t *lib, const char *name) {
  if (lib != NULL && name != NULL) {
    FILE *fl = fopen(name, "rb");
    if (fl != NULL) {
      int res = genglib_load(lib, fl);
      fclose(fl);
      return res;
    }
  }
  genglib_clear(lib);
  return -1;
}


int genglib_save_file (const genglib_t *lib, const char *name) {
  if (lib != NULL && name != NULL) {
    FILE *fl = fopen(name, "wb");
    if (fl != NULL) {
      int res = genglib_save(lib, fl);
      fclose(fl);
      return res;
    }
  }
  return -1;
}
