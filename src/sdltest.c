/* coded by Ketmar // Vampire Avalon (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 */
#include <math.h>
#include <stdlib.h>
#include <stdio.h>

#include "SDL.h"

#include "videolib/videolib.h"
#include "gestureng/gestureng.h"


#define FPS  (30)


static gengesture_t stk;
static gengtemplate_t stkpat;
static int stkpat_inited = 0;
static genglib_t stklib;
static char *message = NULL;
static SDL_TimerID tid_msghide = 0;
static SDL_TimerID tid_frame = 0;
static int frame_changed = 0;


////////////////////////////////////////////////////////////////////////////////
enum {
  EVT_SHOW_FRAME = 1,
  EVT_MESSAGE_RESET
};


////////////////////////////////////////////////////////////////////////////////
static void draw_stroke (const gengesture_t *stk) {
  if (stk->count > 1) {
    Uint32 col = rgb2col(255, 255, 0);
    for (int f = 1; f < stk->count-1; ++f) {
      int x0 = geng_ptx(stk, f-1);
      int y0 = geng_pty(stk, f-1);
      int x1 = geng_ptx(stk, f);
      int y1 = geng_pty(stk, f);
      draw_line(x0, y0, x1, y1, col);
    }
  }
}


static void draw_template (const gengtemplate_t stk) {
  for (int f = 1; f < GENG_PTS_PER_STK; ++f) {
    int g = 255*f/(GENG_PTS_PER_STK-1), b = 255-(255*f/(GENG_PTS_PER_STK-1));
    Uint32 col = rgb2col(0, g, b);
    double x0 = stk[(f-1)*2+0];
    double y0 = stk[(f-1)*2+1];
    double x1 = stk[f*2+0];
    double y1 = stk[f*2+1];
    x0 = x0*200+400;
    y0 = y0*200+300;
    x1 = x1*200+400;
    y1 = y1*200+300;
    draw_line(x0, y0, x1, y1, col);
  }
}


////////////////////////////////////////////////////////////////////////////////
static int stklist_curptr = -1;
static char **stknames = NULL;
static int stknames_count = 0;
static int stknames_maxlen = 0;
static const char *curstkname = NULL;


static void stroke_list_clear (void) {
  for (int f = stknames_count-1; f >= 0; --f) free(stknames[f]);
  free(stknames);
  stklist_curptr = -1;
  stknames = NULL;
  stknames_count = 0;
  stknames_maxlen = 0;
}


static void stroke_list_rebuild (void) {
  int cmp (const void *ps0, const void *ps1) {
    const char **s0 = (const char **)ps0;
    const char **s1 = (const char **)ps1;
    return strcmp(*s0, *s1);
  }

  stroke_list_clear();
  for (int f = 0; f < stklib.count; ++f) {
    int found = 0;
    for (int c = 0; c < stknames_count && !found; ++c) if (strcmp(stklib.names[f], stknames[c]) == 0) found = 1;
    if (!found) {
      stknames = realloc(stknames, sizeof(stknames[0])*(stknames_count+1));
      stknames[stknames_count++] = strdup(stklib.names[f]);
      //fprintf(stderr, "[%s]\n", stknames[stknames_count-1]);
      if (strlen(stklib.names[f]) > stknames_maxlen) stknames_maxlen = strlen(stklib.names[f]);
    }
  }
  qsort(stknames, stknames_count, sizeof(stknames[0]), cmp);
  stknames_maxlen = stknames_maxlen*12+8;
}


static int stroke_list_curptr (int x, int y) {
  if (x >= 4 && y >= 4 && x < stknames_maxlen-8 && y < stknames_count*16+4) return (y-4)/16;
  return -1;
}


static void draw_stroke_list (int curptr) {
  if (stknames_count > 0) {
    fill_rect(0, 0, stknames_maxlen, stknames_count*16+8, rgb2col(0, 0, 127));
  }
  for (int f = 0; f < stknames_count; ++f) {
    Uint32 col, bkcol;
    if (curstkname != NULL && strcmp(stknames[f], curstkname) == 0) {
      col = rgb2col(255, 255, 255);
      bkcol = rgb2col(0, 0, 255);
    } else {
      col = rgb2col(255, 127, 0);
      bkcol = rgb2col(0, 0, 127);
    }
    if (curptr == f) bkcol = rgb2col(0, 127, 0);
    fill_rect(0, f*16+4, stknames_maxlen, 16, bkcol);
    draw_str6(4, f*16+4, stknames[f], col, TRANSPARENT_COLOR);
  }
  if (stknames_count > 0) {
    draw_rect(0, 0, stknames_maxlen, stknames_count*16+8, rgb2col(255, 255, 255));
    draw_rect(1, 1, stknames_maxlen-2, stknames_count*16+6, rgb2col(0, 0, 0));
  }
}


////////////////////////////////////////////////////////////////////////////////
static int show_help = 0;
static const char *help_text[] = {
  "\x1fProtractor demo actions",
  "\x1f-----------------------",
  "\3keyboard:\1",
  " \2F1\1: toggle help",
  " \2F2\1: save library to '\4strokes.dat\1'",
  " \2F3\1: replace library with '\4strokes.dat\1'",
  " \2F8\1: clear library",
  " \2F10\1: quit",
  " \2DEL\1: delete selected stroke",
  "",
  "\3mouse:\1",
  " \2LMB\1: select name or start drawing",
  " \2RMB\1: register current stroke as template for selected name",
  " \2MMB\1: register current stroke as template for new name",
};


static int stlen (const char *str) {
  int len = 0;
  for (; *str; ++str) if ((unsigned char)(str[0]) >= 32) ++len;
  return len;
}


static void stdraw (int x, int y, const char *str) {
  Uint32 fg = rgb2col(255, 255, 255);
  if (str[0] == '\x1f') {
    x = (SCR_WIDTH-stlen(str)*12)/2;
    ++str;
  }
  for (; *str; ++str) {
    unsigned char ch = (unsigned char)(str[0]);
    if (ch < 32) {
      switch (ch) {
        case 1: fg = rgb2col(255, 255, 255); break;
        case 2: fg = rgb2col(0, 255, 0); break;
        case 3: fg = rgb2col(255, 255, 0); break;
        case 4: fg = rgb2col(255, 127, 0); break;
      }
    } else {
      draw_char8(x, y, ch, fg, TRANSPARENT_COLOR);
      x += 12;
    }
  }
}


static void toggle_help (void) {
  show_help = !show_help;
}


static void draw_help (void) {
  if (show_help) {
    int x, y;
    int wdt = 0, hgt = sizeof(help_text)/sizeof(help_text[0])*16+8;
    Uint32 fg = rgb2col(255, 255, 255);
    Uint32 bg = rgb2col(25, 69, 247);
    for (size_t f = 0; f < sizeof(help_text)/sizeof(help_text[0]); ++f) {
      int l = stlen(help_text[f]);
      if (l > wdt) wdt = l;
    }
    wdt = wdt*12+8;
    x = (SCR_WIDTH-wdt)/2;
    y = (SCR_HEIGHT-hgt)/2;
    fill_rect(x, y, wdt, hgt, fg);
    fill_rect(x+1, y+1, wdt-2, hgt-2, rgb2col(0, 0, 0));
    fill_rect(x+2, y+2, wdt-4, hgt-4, bg);
    x += 4;
    y += 4;
    for (size_t f = 0; f < sizeof(help_text)/sizeof(help_text[0]); ++f, y += 16) stdraw(x, y, help_text[f]);
  }
}


////////////////////////////////////////////////////////////////////////////////
static void rebuild_screen (void) {
  fill_rect(0, 0, SCR_WIDTH, SCR_HEIGHT, rgb2col(0, 0, 0));
  draw_stroke(&stk);
  if (stkpat_inited) draw_template(stkpat);
  draw_stroke_list(stklist_curptr);
  if (message != NULL) {
    int w = strlen(message)*16;
    int h = 16;
    int x = (SCR_WIDTH-w)/2;
    int y = SCR_HEIGHT-h-2;
    Uint32 fg = rgb2col(255, 255, 255);
    Uint32 bg = rgb2col(25, 69, 247);
    Uint32 blk = rgb2col(0, 0, 0);
    fill_rect(x-2, y-2, w+4, h+4, fg);
    fill_rect(x-1, y-1, w+2, h+2, blk);
    draw_str8(x, y, message, fg, bg);
  }
  draw_help();
  frame_changed = 1;
}


////////////////////////////////////////////////////////////////////////////////
static Uint32 timer_frame (Uint32 interval, void *param) {
  SDL_Event evt;
  evt.user.type = SDL_USEREVENT;
  evt.user.code = EVT_SHOW_FRAME;
  SDL_PushEvent(&evt);
  return interval;
}


static Uint32 timer_msghide (Uint32 interval, void *param) {
  SDL_Event evt;
  evt.user.type = SDL_USEREVENT;
  evt.user.code = EVT_MESSAGE_RESET;
  SDL_PushEvent(&evt);
  tid_msghide = 0; /* cancelled */
  return 0;
}


static void show_message (const char *msg) {
  if (tid_msghide != 0) SDL_RemoveTimer(tid_msghide);
  if (message != NULL) free(message);
  if (msg && msg[0]) message = strdup(msg); else message = NULL;
  tid_msghide = SDL_AddTimer(5000, timer_msghide, NULL);
  rebuild_screen();
}


static void hide_message (void) {
  if (message != NULL) {
    free(message);
    message = NULL;
    if (tid_msghide != 0) SDL_RemoveTimer(tid_msghide);
    tid_msghide = 0;
    rebuild_screen();
  }
}


////////////////////////////////////////////////////////////////////////////////
static void main_loop (void) {
  SDL_Event event;
  if (genglib_load_file(&stklib, "strokes.dat") == 0) {
    stroke_list_rebuild();
    show_message("'strokes.dat' loaded...");
  }
  rebuild_screen();
  paint_screen();
  while (SDL_WaitEvent(&event)) {
    switch (event.type) {
      case SDL_QUIT: return;
      case SDL_MOUSEMOTION:
        if (SDL_GetWindowGrab(sdl_win)) {
          if ((event.motion.state&SDL_BUTTON(1)) != 0) {
            if (stklist_curptr == -1) {
              geng_add_point(&stk, event.motion.x, event.motion.y);
              rebuild_screen();
            }
          }
        } else if (event.motion.state == 0) {
          int np = stroke_list_curptr(event.motion.x, event.motion.y);
          if (np != stklist_curptr) {
            stklist_curptr = np;
            rebuild_screen();
          }
        }
        break;
      case SDL_MOUSEBUTTONDOWN:
        if (show_help) {
          toggle_help();
        } else {
          if (event.button.button == SDL_BUTTON_LEFT) {
            if (stklist_curptr == -1) {
              geng_clear(&stk);
              SDL_SetWindowGrab(sdl_win, SDL_TRUE);
              geng_add_point(&stk, event.button.x, event.button.y);
              rebuild_screen();
            } else {
              stkpat_inited = 0;
              curstkname = NULL;
              for (int f = 0; f < stklib.count; ++f) {
                if (strcmp(stklib.names[f], stknames[stklist_curptr]) == 0) {
                  geng_copytpl(stkpat, stklib.tpls[f]);
                  curstkname = stklib.names[f];
                  stkpat_inited = 1;
                  break;
                }
              }
            }
          } else if (event.button.button == SDL_BUTTON_RIGHT || event.button.button == SDL_BUTTON_MIDDLE) {
            if (stk.count > 1) {
              const char *nm = (event.button.button == SDL_BUTTON_MIDDLE ? NULL : curstkname);
              geng_normalize(stkpat, &stk);
              stkpat_inited = 1;
              if (nm == NULL) {
                hide_message();
                if ((nm = read_str()) == READSTR_QUIT) return;
              }
              if (nm != NULL) {
                genglib_add_stk(&stklib, nm, &stk);
                stroke_list_rebuild();
                stklist_curptr = stroke_list_curptr(event.button.x, event.button.y);
                geng_clear(&stk);
              }
            }
          }
        }
        rebuild_screen();
        break;
      case SDL_MOUSEBUTTONUP:
        if (event.button.button == SDL_BUTTON_LEFT) {
          if (SDL_GetWindowGrab(sdl_win)) {
            SDL_SetWindowGrab(sdl_win, SDL_FALSE);
            if (stk.count > 1) {
              double score;
              char buf[128];
              curstkname = genglib_find_match(&stklib, &stk, &score);
              if (curstkname != NULL && score < 2.0) curstkname = NULL;
              if (curstkname != NULL) {
                snprintf(buf, sizeof(buf), "%s: score=%.15g", curstkname, score);
              } else {
                snprintf(buf, sizeof(buf), "can't recognize stroke");
              }
              show_message(buf);
            }
          }
        }
        rebuild_screen();
        break;
      case SDL_KEYDOWN:
        switch (event.key.keysym.sym) {
          case SDLK_ESCAPE:
            if (show_help) {
              toggle_help();
              rebuild_screen();
            }
            break;
          case SDLK_F1:
            if (!SDL_GetWindowGrab(sdl_win)) {
              toggle_help();
              rebuild_screen();
            }
            break;
          case SDLK_F2:
            if (genglib_save_file(&stklib, "strokes.dat") == 0) show_message("'strokes.dat' saved...");
            break;
          case SDLK_F3:
            curstkname = NULL;
            if (genglib_load_file(&stklib, "strokes.dat") == 0) show_message("'strokes.dat' loaded...");
            stroke_list_rebuild();
            rebuild_screen();
            break;
          case SDLK_F8:
            curstkname = NULL;
            genglib_clear(&stklib);
            stroke_list_rebuild();
            show_message("library cleared...");
            break;
          case SDLK_r: case SDLK_DELETE: case SDLK_KP_PERIOD:
            if (stklist_curptr >= 0) {
              genglib_remove(&stklib, stknames[stklist_curptr]);
              curstkname = NULL;
              stroke_list_rebuild();
              rebuild_screen();
            }
            break;
          case SDLK_F10: return;
          default: ;
        }
        break;
      case SDL_USEREVENT:
        switch (event.user.code) {
          case EVT_SHOW_FRAME:
            if (frame_changed) {
              frame_changed = 0;
              paint_screen();
            }
            break;
          case EVT_MESSAGE_RESET:
            hide_message();
            break;
        }
        break;
      case SDL_WINDOWEVENT:
        switch (event.window.event) {
          case SDL_WINDOWEVENT_EXPOSED:
            frame_changed = 0;
            paint_screen();
            break;
        }
        break;
      default: ;
    }
  }
}


int main (int argc, char *argv[]) {
  if (video_init()) return 1;
  genglib_init(&stklib);
  geng_init(&stk);
  tid_frame = SDL_AddTimer(1000/FPS, timer_frame, NULL);
  main_loop();
  SDL_RemoveTimer(tid_frame);
  SDL_SetWindowGrab(sdl_win, SDL_FALSE);
  geng_done(&stk);
  genglib_done(&stklib);
  stroke_list_clear();
  if (message != NULL) free(message);
  return 0;
}
